import subprocess
from subprocess import Popen, PIPE
import time
import os
import zmq
import time
import sys
import threading
import graphyte
import ITSDAQ 
from zmq_client import Client
try:
    import queue
except ImportError:
    import Queue as queue
    
class ServerThread(threading.Thread):
    def __init__(self,threadID):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.lock = threading.Lock()
        self.comm_queue=queue.Queue()
        self.sensor_thread=ModuleMonitor('127.0.0.1',self.comm_queue)

    def run(self):
        print("Starting " + str(self.threadID))
        self.server()

    def server(self):
        port = "5555"
        context = zmq.Context()
        socket=context.socket(zmq.REP)
        socket.bind("tcp://*:%s" %port)
        while True:
            message = str(socket.recv())
            print( "recieved message: ",message)
            if "break" in message.lower():
                socket.send("Goodbye!".encode())
                break
            elif 'sensor' in message.lower():
                data=message.split(" ")
                if('on' in data):
                    print("turned on sensor")
                    self.sensor_thread.start()
                else:
                    print("turned off sensor")
                    self.comm_queue.put('break')
                socket.send("ran module sensors".encode())
            elif('power' in message.lower()):
                 if('on' in message):
                     print("turning on amac")
                     ITSDAQ.StartAmac()
                 else:
                     print("turnng off amac")
                     ITSDAQ.StopAmac()
            elif "run_test" in message.lower():
                data=message.split(" ")[1:]
                test_data=[bl.lower()=='true' for bl in data] #convert fromm array of strings to array of bools
                print("running tests",test_data)
                ITSDAQ.run_tests(test_data)
                socket.send("Running  tests".encode())
            else:
                socket.send("Good to go!".encode())

class ModuleMonitor(threading.Thread):
     def __init__(self,grafana_ip,comm_queue):
         threading.Thread.__init__(self)
         self.comm_queue=comm_queue
         self.grafana_ip=grafana_ip

     def run(self):
         self.sensors()

     def sensors(self):
         graphyte.init(self.grafana_ip)
         command=''
         while True:
            if (not self.comm_queue.empty()):
                command=str(self.queue.get())
                if 'break' in command.lower():
                    break
            try:
                ntc_data=ITSDAQ.read_hybrid_ntc()
                print("hybrid NTC data",ntc_data)
                #graphyte.send("left hybrid ntc",int(ntc_data[0]))
                #graphyte.send("right hybrid ntc",int(ntc_data[1]))
            except Exception as e:
                print("hybrid ntc data error")
                print(e)
            time.sleep(.01)            
            try:
                amac_ntc=ITSDAQ.read_pb_ntc()
                print("amac ntc data",amac_ntc)
                #graphyte.send("power board ntc",int(amac_ntc))
            except Exception as e:
                print("powerboard ntc failure")
                print(e)
            time.sleep(5)

if __name__=='__main__':            
    Thread=ServerThread("ITS server")
    Thread.start()
