import piplates.DAQCplate as DAQ
import math
from Sensor import Sensor
class HIH(Sensor):
        def __init__(self,input_pin=0,vdd=3.3):
                Sensor.__init__("Hih")
		self.input_pin=input_pin
		self.vdd=vdd
		
	def read_rh(self,temp):
		Vin=DAQ.getADC(0,self.input_pin)
		sensor_RH=(Vin/vdd-0.1515)/0.00636
		True_RH = (Sensor_RH)/(1.0546*0.00216*Temp)
		return True_RH
        
        def get_data(self,temp):
                humidity=self.read_rh(temp)
                self.data={"Humidity":humidity}
                return self.data
