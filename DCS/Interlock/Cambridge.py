import serial
import time
import re

#cambridge interlock unit
#untested for now

class Cambridge:
    def __init__(self,location="/dev/ttyACM0",baudrate=115200,outtime=2):
        try:
            self.interlock=serial.Serial(location,baudrate,timeout=outtime)
            self.interlock.write("ena,100\r")
            self.h_ntc_number=0
            self.u_ntc_number=0
        except:
            print("unable to open interlock")
        self.commands={"htl":"hybdrid temp lower limit",
                       "hth":"hybdrid temp upper limit",
                       "hne":"Number of Hybdrid sensors",
                       "hna":"Number of Samples for average",
                       "hrn":"Nominal hybdrid thermistor resistance ",
                       "htn":"Nominal thermistor temperature",
                       "hbn":"Hybdrid Thermistor B parameter",
                       "utl":"User temp lower limit",
                       "uth":"User temp upper limit",
                       "une":" Number of user sensors",
                       "una":"User Number of Samples for average",
                       "urn":"User Nominal hybdrid thermistor resistance ",
                       "utn":"User Nominal thermistor temperature",
                       "ubn":"User Thermistor B parameter",
                       "rtl":"SHT temp lower limit",
                       "rth":"SHT temp upper limit",
                       "rrl":"SHT Humidity lower limit",
                       "rrh":"SHT Humidity Upper limit",
                       "rne":"Number of SHT sensors"}
        
        
    def get_config(self):
        self.interlock.write("irs, 100\r".encode())
        data=self.interlock.readline()
        config=data[:3]
        try:
            data.replace(config,self.commands[config])
        except KeyError:
            pass
        print(data)
        print(config)
        while data:
            data=self.interlock.readline().decode()
            config=data[:3]
            try:
                data=data.replace(config,self.commands[config])
                if config=='une':
                    self.u_ntc_number=int(re.findall(r"\d",message)[0])
                elif config=='hne':
                    self.h_ntc_number=int(re.findall(r"\d",message)[0])

            except KeyError:
                pass
            print(data[:-2])
        
            
    def set_humidity_range(self,upper_limit=30,lower_limit=0):
        query="rrl, "+str(lower_limit)+"\r"
        self.interlock.write(query.encode())
        data=self.interlock.readline()
        while data:
            print(data)
            data=self.interlock.readline()
        query="rrh, "+str(upper_limit)+"\r"
        self.interlock.write(query.encode())
        data=self.interlock.readline()
        while data:
            print(data)
            data=self.interlock.readline()


    def set_hybrid_NTC_temp(self,upper_limit=30,lower_limit=-25):
        query="htl, "+str(lower_limit)+"\r"
        self.interlock.write(query.encode())
        print(self.interlock.readline())
        query="hth, "+str(upper_limit)+"\r"
        self.interlock.write(query.encode())
        data=self.interlock.readline()
        while data:
            print(data)
            data=self.interlock.readline()
        
    def set_hybrid_user_temp(self,upper_limit=30,lower_limit=-25):
        query="utl,"+str(lower_limit)+"\r"
        self.interlock.write(query.encode())
        print(self.interlock.readline())
        query="uth,"+str(upper_limit)+"\r"
        self.interlock.write(query.encode())
        print(self.interlock.readline())
        

    def set_SHT_temp(self,upper_limit=30,lower_limit=-25):
        query="rtl,"+str(lower_limit)+"\r"
        self.interlock.write(query.encode())
        print(self.interlock.readline())
        query="rth,"+str(upper_limit)+"\r"
        self.interlock.write(query.encode())
        print(self.interlock.readline())

    def set_B_parameters(self,user=3000,hybrid=3000):
        query="hbn,"+str(hybrid)+"\r"
        self.interlock.write(query.encode())
        self.interlock.readline()
        query="ubn,"+str(hybrid)+"\r"
        self.interlock.write(query.encode())
        self.interlock.readline()
        
    def disable(self):
        self.interlock.write("dis,100\r")
        data=self.interlock.readline()
        while data:
            print(data)
            data=self.interlock.readline()

    def enable(self):
        self.interlock.write("ena,100,\r")
        data=self.interlock.readline()
        while data:
            print(data)
            data=self.interlock.readline()

    def enable_NTC(self,hybrid=0,user=0):
        query="hne,"+str(hybrid)+"\r"
        self.interlock.write(query.encode())
        data=self.interlock.readline()
        while data:
            print(data)
            data=self.interlock.readline()
        query="hne,"+str(user)+"\r"
        self.interlock.write(query.encode())
        data=self.interlock.readline()
        while data:
            print(data)
            data=self.interlock.readline()

    def get_data(self):
        data_dict={}
        self.interlock.write("idr, 100 \r")
        read_data=self.interlock.readline()
        while read_data:
            data=read_data.decode()
            read_data=self.interlock.readline()
            print(read_data)
            read_data=str(data)
            if(read_data!= '' and read_data !='\r'):
                data=read_data.split(',')
                
        for i in range(self.h_ntc_number):
            label="hybrid_ntc"+str(i)
            data_dict[label]=data[i]
        
if __name__=="__main__":
    #for testing
    cambridge=Interlock()
    cambridge.get_config()

