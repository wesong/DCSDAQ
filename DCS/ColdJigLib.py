from __future__ import print_function
import time
import graphyte
import threading
import thread
from zmq_client import Client
import re
import sht_sensor
try:
    from queue import queue
except ImportError:
    from Queue import Queue as queue

from DCSTools import message_parser


#local configuration
from initialize import SensorDict
from initialize import master,grafana
from initialize import dcs_dict
from initialize import IVCurveConfig
import HVTools


CommQueue= queue()
CycleQueue=queue()
def start_sensors():
    sensor_thread=SensorThread("sensor thread",CommQueue,Chiller=dcs_dict['Chiller'],interlock=dcs_dict['interlock'],LV=dcs_dict['LV'],HV=dcs_dict['HV'])
    sensor_thread.start()

def stop_sensors():
    CommQueue.put("break")
                
def do_IVcurve():
    #run IV curve test in seperate thread
    step,end,start=IVCurveConfig()
    thread.start_new_thread(HVTools.IVCurve, (self.HV,step,end,start,))
 
def LV_TurnOn():
    try:
        dcs_config['LV'].TurnOn()
        return True
    except:
        return False

def LV_TurnOff():
    try:
        dcs_config['LV'].TurnOff()
        return True
    except:
        return False

def LV_SetVoltage(value,channel):
    try:
        dcs_config['LV'].SetVoltage(value,channel)
        return True
    except:
        return False
    
def LV_SetCurrent(value,channel):
    try:
        dcs_config['LV'].SetCurrent(value,channel)
        return True
    except:
        return False
    
def LV_SetVoltageProtection(value,channel):
    try:
        dcs_config['LV'].SetVoltageProtection(value,channel)
        return True
    except:
        return False
    

def LV_GetCurrent(channel):
    try:
        current=dcs_config['LV'].GetCurrent(channel)
        return current
    except:
        return None
    
def LV_GetVoltage(channel):
    try:
        voltage=dcs_config['LV'].GetVoltage(channel)
        return voltage
    except:
        return None
    
def HV_TurnOn():
    try:
        dcs_config['HV'].TurnOn()
        return True
    except:
        return None
def HV_TurnOff():
    try:
        dcs_config['HV'].TurnOff()
        return True
    except:
        return None
    
def HV_SetCompliance(value):
    try:
        dcs_config['HV'].SetCurrentCompliance(value)
        return True
    except:
        return None

def HV_SetRange(voltage_range):
    try:
        dcs_config['HV'].SetVoltageRange(value)
        return True
    except:
        return None
    
def HV_SetVoltage(value):
    try:
        dcs_config['HV'].SetVoltageLevel(value)
        return True
    except:
        return None
    
def HV_SetCurrent(value):
    try:
        dcs_config['HV'].SetCurrentLevel(value)
        return True
    except:
        return None
    
def HV_GetVoltage():
    try:
        current=dcs_config['HV'].GetVoltage()
        return current
    except:
        return None
def HV_GetCurrent():
    try:
        current=dcs_config['HV'].GetCurrent()
        return current
    except:
        return None
    
def Chiller_SetTemperature(value):
    try:
        dcs_config['Chiller'].SetTemperature(value)
        return True
    except:
        return False
    
def Chiller_TurnOn():
    try:
        dcs_config['Chiller'].TurnOn()
        return True
    except:
        return False
def Chiller_TurnOff():
    try:
        dcs_config['Chiller'].TurnOff()
        return True
    except:
        return False
def Chiller_GetTemperature():
    try:
        temp=dcs_config['Chiller'].GetTemperature()
        return temp
    except:
        return None


def Start_ThermalCyle():
    DAQ_client=zmq_client()
    cycler=ThermalCycle("Thermal_Cycle",CycleQueue,DAQ_client,sensor,dcs_config['Chiller'])
                            
class SensorThread(threading.Thread):
    def __init__(self,threadID,comm_queue,Chiller=None,interlock=None,LV=None,HV=None):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.chiller = Chiller
        self.queue=comm_queue
        self.lock = threading.Lock()
        self.interlock=interlock
        global master
        global grafana
        self.LV=LV
        self.HV=HV
        graphyte.init(grafana)
        self.MasterServer=Client(master,"5554")
    

    def run(self):
        print ("Starting " + str(self.threadID))
        self.Read()

    def Read(self):
        command=""
        while True:
            if (not self.queue.empty()):
                command=str(self.queue.get())
                print("from queue", command)
                if 'break' in command.lower():
                    break
            with self.lock:
                try:
                    chiller_t = self.chiller.GetTemperature()
                    graphyte.send("QCBox.Chiller_Temperature",float(chiller_t))
                except :
                    print("chiller not responding")
                try:
                    lv_voltage=self.LV.GetActualVoltage()
                    lv_current=self.LV.GetActualCurrent()
                    print(type(lv_voltage))
                    print(float(str(lv_voltage)))
                    graphyte.send("QCBox.LVVoltage",float(lv_voltage))
                    graphyte.send("QCBox.LVCurrent",float(lv_current))
                except Exception as e:
                    print(str(e))
                    print("low voltage not responding")

            with self.lock:
                try:
                    data=self.interlock.get_data()
                    data=data.split(",")
                    metrics=["hybrid NTC 1",
                             "hybrid NTC 2",
                             "hybrid NTC 3",
                             "hybrid NTC 4",
                             "hybrid NTC 5",
                             "hybrid NTC 6",
                             "hybrid NTC 7",
                             "hybrid NTC 8",
                             "user NTC 1",
                             "user NTC 2",
                             "SHT_temp",
                             "SHT_humidity"]
                    
                    for point,metric in zip(data,metrics):
                        point=int(point)
                        name="interlock."+metric
                        graphyte.send(name,point)
                except AttributeError:
                    print("interlock not responding")
                    
            for label,sensor in SensorDict.items():
                try:
                    sensor_items=sensor.get_data().items()
                except:
                    print("problem with "+label)
                    continue
                for name,data in sensor_items:
                    try:
                        grafana_name='QCBox.'+str(label)
                        grafana_name+="."+name
                        print(grafana_name,data)
                        graphyte.send(grafana_name,float(data))
                    except (sht_sensor.sensor.ShtCRCCheckError,sht_sensor.sensor.ShtCommFailure):
                        print("SHT sensor in config file but not connected properly")
                    except AttributeError:
                        print("encountered a problem with "+label)
            time.sleep(5)


class ThermalCycle(threading.Thread):
    def __init__(self,threadID,comm_queue,DAQ_client,sensor,Chiller,total_cycles=10,target_temp=-40,room_temperature=25):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.chiller = Chiller
        self.queue=comm_queue
        self.lock = threading.Lock()
        self.DAQ_client=DAQ_client
        self.target_temp=target_temp
        self.room_temperature=room_temperature
        self.total_cycles=total_cycles
        
    def run(self):
        print ("Starting " + str(self.threadID))
        self.loop()
        
    def loop(self):
        Cycle='High'
        number=0
        while number<self.total_cycles:
            temp=sensor.get_data['Temperature']
            if (not self.queue.empty()):
                command=str(self.queue.get())
                print("from queue", command)
                if 'break' in command.lower():
                    break
                if 'change_temp' in command.lower:
                    if Cycle=='High' and temp >= self.room_temperature:
                        self.Chiller.SetTemperature(self.target_temperature)
                        Cycle='Low'
                        number+=1
                    if Cycle=='Low' and temp <=self.target_temperature:
                        self.Chiller.SetTemperature(self.room_temperature)
                        Cycle='High'
                        
        
