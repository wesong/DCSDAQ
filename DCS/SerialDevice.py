import serial

class SerialDevice:
    def __init__(self,location="/dev/ttyUSB0",baudrate=9600,outtime=5):
        try:
            self.device=serial.Serial(location,baudrate,timeout=outtime)
        except serial.SerialException:
            print("can't open device, is it connected on "+location+" ?")

    def GetType(self):
        query="*IDN?\n"
        try:
            self.device.write(query.encode())
            reply=self.device.readline()
            if reply=='':
                raise serial.SerialTimeoutException
            else:
                print(reply)    
        except serial.SerialTimeoutException:
            print("query from GetType() failed")
            
