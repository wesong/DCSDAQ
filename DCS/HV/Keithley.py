import serial
import time

class Keithley:
    
    def __init__(self,location="/dev/ttyUSB0",baudrate=9600,outtime=5):
        try:
            self.device=serial.Serial(location, baudrate, timeout=outtime)
        except serial.SerialException:
            print("can't open the keithley, is it connected on "+location+" ?")

    def __del__(self):
        try:
            self.device.close()
        except AttributeError:
            pass
        
    def GetType(self):
        query="*IDN?\n"
        
        self.device.write(query.encode())
        idn=self.device.readline()
        print(idn)
        return idn
       

        
    def TurnOn(self):
        query=":OUTP ON\n"
        try:
            self.device.write(query.encode())
            print(self.device.readline())
        except serial.SerialTimeoutException:
            print("Device turn on failed")

    def TurnOff(self):
        query=":OUTP OFF\n"
        try:
            self.device.write(query.encode())
            print(self.device.readline())
        except serial.SerialTimeoutException:
            print("Device turn off failed")
            
    def SetCurrentCompliance(self,compliance):
        try:
            query=":SENS:CURR:PROT "+str(compliance)+"\n"
            self.device.write(query.encode())
            print(self.device.readline())
        except serial.SerialTimeoutException:
            print("Device setting compliance current to "+str(compliance)+" failed")
            
    def SetVoltageCompliance(self,compliance):
        try:
            query=":SENS:VOLT:PROT "+str(compliance)+"\n"
            self.device.write(query.encode())
            print(self.device.readline())
        except serial.SerialTimeoutException:
            print("Device setting compliance voltage to "+str(compliance)+" failed")

    def ReadData(self):
        try:
            query=":READ?\n"
            self.device.write(query.encode())
            data=self.device.readline()
            print(data)
            return data
        except serial.SerialTimeoutException:
            print("Device Read failed")
            return None
    def SetVoltageRange(self,volt):
        try:
            query=":SOUR:VOLT:RANG "+ str(volt)+"\n"
            self.device.write(query.encode())
            print(self.device.readline())
        except serial.SerialTimeoutException:
            print("Device voltage range failed")

    def SetCurrentRange(self,current):
        try:
            query=":SOUR:CURR:RANG "+ str(current)+"\n"
            self.device.write(query.encode())
            print(self.device.readline())
        except serial.SerialTimeoutException:
            print("Device current range failed")

    def SetVoltageLevel(self,volt):
        try:
            query=":SOUR:VOLT:LEV "+str(volt)+"\n"
            self.device.write(query.encode())
        except serial.SerialTimeoutException:
            print("Device voltage level failed")


    def SetCurrentLevel(self,current):
        try:
            query=":SOUR:CURR:LEV "+str(current)+"\n"
            self.device.write(query.encode())
            print(self.device.readline())
        except serial.SerialTimeoutException:
            print("Device current level failed")

    def GetCurrent(self):
        try:
            query=":MEAS:CURR?\n"
            self.device.write(query.encode())
            results=self.device.readline()
            current=results.split(",")[1]
            return float(current)
        except serial.SerialTimeoutException:
            print("Device get current failed")

    def GetVoltage(self):
        try:
            query=":MEAS:VOLT?\n"
            self.device.write(query.encode())
            results=self.device.readline()
            voltage=results.split(",")[0]
            return float(voltage)
        except serial.SerialTimeoutException:
            print("Device get voltage failed")
            return ""
    def GetResistance(self):
        try:
            query=":MEAS:RES?\n"
            self.device.write(query.encode())
            return self.device.readline()
        except serial.SerialTimeoutException:
            print("Device get resistance failed")
            return ""
    #to implement, switch between front/rear panel
    def ToRear(self):
        query=":ROUT:TERM REAR\n"
        self.device.write(query.encode())

    def ToFront(self):
        query=":ROUT:TERM FRON\n"
        self.device.write(query.encode())
        
if __name__ == "__main__":
    kth=Keithley()
    
    kth=Keithley()
    kth.GetType()
    kth.SetCurrentRange(1e-5)
    kth.SetCurrentCompliance(3e-7)
    kth.SetVoltageCompliance(20)
    kth.TurnOn()
    kth.ReadData()
    kth.SetCurrentRange(1e-5)
    time.sleep(2)
    kth.SetVoltageLevel(10)
    kth.ReadData()
    time.sleep(1)
    kth.SetVoltageLevel(15)
    kth.GetResistance()

    kth.TurnOff()
